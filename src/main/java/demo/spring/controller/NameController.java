package demo.spring.controller;

import demo.spring.NameLookup;
import demo.spring.NameRequest;
import demo.spring.service.NameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class NameController {

	private NameService nameService;
	
	@MessageMapping("/message")
	@SendToUser
	public NameLookup getName(NameRequest nameRequest) {
		return nameService.getName(nameRequest.getFirstName(), 
				nameRequest.getLastName());
	}

//	@MessageExceptionHandler
//	@SendToUser(value = "/queue/errors", broadcast = false)
//	String handleException(Exception e)
//	{
//		return "caught ${e.message}";
//	}


	@Autowired
	public void setNameService(NameService nameService) {
		this.nameService = nameService;
	}
	
}
