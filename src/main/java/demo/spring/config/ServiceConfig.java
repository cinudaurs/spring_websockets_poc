package demo.spring.config;

import demo.spring.service.NameService;
import demo.spring.service.NinjaNameService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

	@Bean
	public NameService nameService() {
		return new NinjaNameService();
	}
	
}
